 package com.sigmotoa.codes.workshop;

import java.util.Scanner;

/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
public class Convertion {

    //convert of units of Metric System

//Km to metters
    public static double kmToM1(double km){

        double KM, metros;
		KM=km;
		metros=km/100;
        System.out.println("de metros a Kilometros es "+metros);        
        
        return  metros;
    }

//Km to metters
    public static double kmTom(double metros){
        
        double KM, metro;
		metro=metros;
                KM=metros*100;
                System.out.println("kilometros a metros es "+KM);
        return KM;
    }
    
    //Km to cm
    public static double kmTocm(double km){
        
        double KM,cm;
        KM=km;
        cm=km*1000;
        System.out.println("de kilometros a centimetros "+cm);
        
        return cm;
    }

//milimetters to metters
    public static double mmTom(int mm){
        
        double metros,mim;
        mim=mm;
        metros=mm/1000;
        System.out.println("de milimetros a metros"+metros);
        
        
        return metros;
    }
//convert of units of U.S Standard System

//convert miles to foot
    public static double milesToFoot(double miles){
        
        double millas,pie;
        millas=miles;
        pie=miles*5280;
        
        System.out.println("de millas a pies "+pie);
        return pie;
    }

//convert yards to inches
    public static int yardToInch(int yard){
    
        int yar,pulgada;
        yar=yard;
        pulgada=yard*3;
        System.out.println("de yardas a pulgadas"+pulgada);
        
        return pulgada;
    }
    
    //convert inches to miles
    public static double inchToMiles(double inch){
       
        double inc,miles;
        inc=inch;
        miles=inch/63360;
        System.out.println("de pulgadas a millas"+miles);
        
        return miles;
    }
//convert foot to yards
    public static int footToYard(int foot){
        
        int yarda;
        yarda=foot/3;
        System.out.println("de pie a yarda "+yarda);
        return yarda;
    }

//Convert units in both systems

//convert Km to inches
    public static double kmToInch(double km){
    
    double pulgada;
    pulgada=km*39370.079;
    System.out.println("de kilometros a pulgadas");
    return pulgada;
    }

//convert milimmeters to foots
    public static double mmToFoot(String mm)
    {return 0.0;}
//convert yards to cm    
    public static double yardToCm(String yard)
    {return 0.0;}


}
